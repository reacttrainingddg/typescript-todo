import * as React from 'react';
import { connect } from 'react-redux';

import { ApplicationState, actions, selectors } from '../../Redux/Store';
import TodoList from '../../Components/TodoList/TodoList';

const mapStateToProps = (state: ApplicationState) => ({
  todos: selectors.Data.Todos.todos(state),
});

const mapDispatchToProps = {
  addTodo: actions.Data.Todos.addTodo,
  removeTodoById: actions.Data.Todos.removeTodoById,
  toggleIsDone: actions.Data.Todos.toggleIsDone,
};

type TodoListContainerProps = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

class TodoListContainer extends React.PureComponent<TodoListContainerProps> {
  public render(): JSX.Element {
    return (
      <TodoList
        todos={this.props.todos}
        removeTodoById={this.props.removeTodoById}
        toggleIsDone={this.props.toggleIsDone}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodoListContainer);
