import * as React from 'react';
import { connect } from 'react-redux';

import { actions, ApplicationState } from '../../Redux/Store';
import AddTodo from '../../Components/AddTodo/AddTodo';

const mapStateToProps = (state: ApplicationState) => ({});

const mapDispatchToProps = {
  addTodo: actions.Data.Todos.addTodo,
  removeAll: actions.Data.Todos.removeAll,
  removeDone: actions.Data.Todos.removeDone,
};

type AddTodoContainerProps = ReturnType<typeof mapStateToProps> & typeof mapDispatchToProps;

class AddTodoContainer extends React.PureComponent<AddTodoContainerProps> {
  public render(): JSX.Element {
    return (
      <AddTodo onAdd={this.props.addTodo} onRemoveAll={this.props.removeAll} onRemoveDone={this.props.removeDone} />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddTodoContainer);
