import { ActionsUnion, createAction } from '@sandstormmedia/react-redux-ts-utils';
import { createSelector } from 'reselect';
import * as Immutable from 'immutable';

import { ApplicationState } from '../../Store';
import Todo, { createTodo } from '../../../Domain/Todo';

//
// State
//
export interface TodosState {
  // example of normalized state
  // https://redux.js.org/recipes/structuring-reducers/normalizing-state-shape
  // It's a bit overkill for a simple one-dimensional data structure like this,
  // but it scales really well!
  // Usually one would use a simple 'ReadonlyArray<Todo>' here.
  allIds: ReadonlyArray<string>;
  byId: {
    [id: string]: Todo;
  };
}

const initialState: TodosState = {
  allIds: [],
  byId: {},
};

//
// Actions
//
export enum ActionTypes {
  ADD_TODO = '@@salt-solutions/Todos/ADD_TODO',
  REMOVE_ALL = '@@salt-solutions/Todos/REMOVE_ALL',
  REMOVE_TODO_BY_ID = '@@salt-solutions/Todos/REMOVE_TODO_BY_ID',
  TOGGLE_IS_DONE = '@@salt-solutions/Todos/TOGGLE_IS_DONE',
  REMOVE_DONE = '@@salt-solutions/Todos/REMOVE_DONE',
}

export const actions = {
  addTodo: (newTodoTitle: string) => createAction(ActionTypes.ADD_TODO, { newTodoTitle }),
  removeAll: () => createAction(ActionTypes.REMOVE_ALL),
  removeTodoById: (id: string) => createAction(ActionTypes.REMOVE_TODO_BY_ID, { id }),
  toggleIsDone: (id: string) => createAction(ActionTypes.TOGGLE_IS_DONE, { id }),
  removeDone: () => createAction(ActionTypes.REMOVE_DONE),
};

type TodosActions = ActionsUnion<typeof actions>;

//
// Reducer
//
export function reducer(state: TodosState = initialState, action: TodosActions): TodosState {
  switch (action.type) {
    case ActionTypes.ADD_TODO: {
      const newTodo = createTodo(action.payload.newTodoTitle);
      return {
        ...state,
        allIds: state.allIds.concat(newTodo.id),
        byId: { ...state.byId, [newTodo.id]: newTodo },
      };
    }
    case ActionTypes.REMOVE_ALL:
      return { ...state, allIds: [], byId: {} };
    case ActionTypes.REMOVE_TODO_BY_ID:
      return {
        ...state,
        allIds: state.allIds.filter((id) => id !== action.payload.id),
        byId: Immutable.remove(state.byId, action.payload.id),
      };
    case ActionTypes.TOGGLE_IS_DONE:
      return {
        ...state,
        byId: Immutable.update(state.byId, action.payload.id, (todo): Todo => ({ ...todo, isDone: !todo.isDone })),
      };
    case ActionTypes.REMOVE_DONE: {
      const newAllIds = state.allIds.filter((id) => !state.byId[id].isDone);

      return {
        ...state,
        allIds: newAllIds,
        byId: newAllIds.reduce((acc, id) => ({ ...acc, [id]: state.byId[id] }), {}),
      };
    }
    default:
      return state;
  }
}

//
// Selectors
//
const byIdSelector = (state: ApplicationState) => state.Data.Todos.byId;
const allIdsSelector = (state: ApplicationState) => state.Data.Todos.allIds;

const todosSelector = createSelector([allIdsSelector, byIdSelector], (allIds, byId) => allIds.map((id) => byId[id]));

export const selectors = {
  todos: todosSelector,
};

// TODO use epics to fetch todos from an API
//
// fetch('https://jsonplaceholder.typicode.com/todos')
//     .then(response => response.json())
//     .then(json => console.log(json));
