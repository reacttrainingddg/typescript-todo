import { combineReducers } from 'redux';

import * as TodosStore from './TodosStore';

export interface DataState {
  Todos: TodosStore.TodosState;
}

export const actions = {
  Todos: TodosStore.actions,
};

export const selectors = {
  Todos: TodosStore.selectors,
};

export const reducer = combineReducers({
  Todos: TodosStore.reducer,
});
