import { combineReducers } from 'redux';

import * as DataStore from './Data';

export interface ApplicationState {
  Data: DataStore.DataState;
}

export const actions = {
  Data: DataStore.actions,
};

export const selectors = {
  Data: DataStore.selectors,
};

export const rootReducer = combineReducers({
  Data: DataStore.reducer,
});
