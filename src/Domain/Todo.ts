export default interface Todo {
  id: string;
  isDone: boolean;
  label: string;
}

const createTodoId: () => string = () => ''.concat(Date.now().toString(), (Math.random() * 100000).toString());

export function createTodo(label: string): Todo {
  return {
    id: createTodoId(),
    isDone: false,
    label,
  };
}
