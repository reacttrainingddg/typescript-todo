import React, { createRef } from 'react';
import composeClassNames from 'classnames';

import Button from '../Button/Button';
import TextInput from '../TextInput/TextInput';
import styles from './AddTodo.module.scss';

//
// Props
//
interface AddTodoProps {
  className?: string;
  onAdd: (newTodoTitle: string) => void;
  onRemoveAll: () => void;
  onRemoveDone: () => void;
}

//
// Class
//
class AddTodo extends React.PureComponent<AddTodoProps> {
  private textInputRef: React.RefObject<HTMLInputElement>;

  constructor(props: AddTodoProps) {
    super(props);

    this.textInputRef = createRef<HTMLInputElement>();
  }

  public render(): JSX.Element {
    const { className } = this.props;
    const finalClassName = composeClassNames(styles['add-todo'], className);

    return (
      <div className={finalClassName}>
        <TextInput
          forwardedRef={this.textInputRef}
          onEnterKey={this.handleAddTodo}
          placeholder="Title"
          autofocus={true}
        />
        <Button label="Add Todo" onClick={this.handleAddTodo} />
        <Button label="Remove All" onClick={this.handleRemoveAll} />
        <Button label="Remove Done" onClick={this.handleRemoveDone} />
      </div>
    );
  }

  private handleAddTodo = () => {
    if (this.textInputRef.current !== null && this.textInputRef.current.value !== '') {
      this.props.onAdd(this.textInputRef.current.value);
    }
  }

  private handleRemoveAll = () => {
    this.props.onRemoveAll();
  }

  private handleRemoveDone = () => {
    this.props.onRemoveDone();
  }
}

export default AddTodo;
