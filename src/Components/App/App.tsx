import React from 'react';

import TodoListContainer from '../../Containers/TodoListContainer/TodoListContainer';
import AddTodoContainer from '../../Containers/AddTodoContainer/AddTodoContainer';
import logo from './logo.svg';
import styles from './App.module.scss';

class App extends React.PureComponent {
  public render(): JSX.Element {
    return (
      <div className={styles.app}>
        <header className={styles.app__header}>
          <img src={logo} className={styles.app__logo} alt="logo" />
          <h2>Todo List</h2>
        </header>
        <main className={styles.app__container}>
            <AddTodoContainer />
            <TodoListContainer />
        </main>
      </div>
    );
  }
}

export default App;
