import React from 'react';

import TodoListItem from '../TodoListItem/TodoListItem';
import Todo from '../../Domain/Todo';
import styles from './TodoList.module.scss';

interface TodoListProps {
  todos: ReadonlyArray<Todo>;
  removeTodoById: (id: string) => void;
  toggleIsDone: (id: string) => void;
}

class TodoList extends React.PureComponent<TodoListProps> {
  public render(): JSX.Element {
    const { todos } = this.props;

    return <ul className={styles['todo-list']}>{this.renderTodos(todos)}</ul>;
  }

  private renderTodos = (todos: ReadonlyArray<Todo>) =>
    todos.map((todo, index) => (
      <TodoListItem
        key={todo.id}
        label={todo.label}
        isDone={todo.isDone}
        onRemove={this.createRemoveHandler(todo.id)}
        onToggleIsDone={this.createToggleIsDoneHandler(todo.id)}
      />
    ))

  private createRemoveHandler = (id: string) => () => this.props.removeTodoById(id);
  private createToggleIsDoneHandler = (id: string) => () => this.props.toggleIsDone(id);
}

export default TodoList;
