import React from 'react';
import composeClassName from 'classnames';
import { PickDefaultProps } from '@sandstormmedia/react-redux-ts-utils';

interface TextInputProps {
  /**
   * An optional className to render on the input.
   */
  className?: string;

  /**
   * HTML Input attribute: type
   */
  type: string;

  /**
   * Flag to indicate if the input is disabled.
   */
  isDisabled?: boolean;

  /**
   * HTML Input attribute: placeholder
   */
  placeholder?: string;

  /**
   * An optional handler that is triggered by pressing the enter key inside the input.
   */
  onEnterKey?: () => void;

  /**
   * An optional forwarded reference to access the rendered input element from the ouside.
   */
  forwardedRef: React.RefObject<HTMLInputElement>;

  /**
   * HTML Input attribute: autofocus
   */
  autofocus?: boolean;
}

const textInputDefaultProps: PickDefaultProps<TextInputProps, 'type'> = {
  type: 'text',
};

class TextInput extends React.PureComponent<TextInputProps> {
  public static defaultProps = textInputDefaultProps;

  public render(): JSX.Element {
    const { type, placeholder, isDisabled, className, forwardedRef, autofocus } = this.props;
    const finalClassNames = composeClassName(
      'input',
      {
        ['input--disabled']: isDisabled,
      },
      className,
    );

    return (
      <input
        ref={forwardedRef}
        className={finalClassNames}
        placeholder={placeholder}
        disabled={isDisabled}
        type={type}
        onKeyPress={this.handleKeyPress}
        autoFocus={autofocus}
      />
    );
  }

  private handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const { onEnterKey } = this.props;

    if (onEnterKey !== undefined) {
      const enterKeyCode = 13;
      const keyCode = event.keyCode || event.which;

      if (keyCode === enterKeyCode) {
        onEnterKey();
      }
    }
  }
}

export default TextInput;
