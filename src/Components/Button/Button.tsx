import React from 'react';
import composeClassName from 'classnames';

import { PickDefaultProps } from '@sandstormmedia/react-redux-ts-utils';
import styles from './Button.module.scss';

interface ButtonProps {
  /**
   * Optional className for additional customization.
   */
  className?: string;

  /**
   * Label to be displayed on the button
   * TODO: Maybe use ReactNode to allow for buttons with icons or composed labels.
   */
  label: string;

  /**
   * Click handler that handles every click on the button
   */
  onClick?: () => void;

  /**
   * Flag to mark the button as disabled.
   */
  isDisabled: boolean;
}

const buttonDefaultProps: PickDefaultProps<ButtonProps, 'isDisabled'> = {
  isDisabled: false,
};

/**
 * Button Component
 */
class Button extends React.PureComponent<ButtonProps> {
  public static defaultProps = buttonDefaultProps;

  public render(): JSX.Element {
    const { isDisabled, label, className } = this.props;
    const finalClassName = composeClassName(
      styles.button,
      {
        [styles['button--disabled']]: isDisabled,
      },
      className,
    );

    return (
      <button className={finalClassName} disabled={isDisabled} onClick={this.handleClick}>
        {label}
      </button>
    );
  }

  private handleClick = () => {
    if (this.props.onClick !== undefined) {
      this.props.onClick();
    }
  }
}

export default Button;
