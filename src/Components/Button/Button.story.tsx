import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from './Button';

storiesOf('Button', module)
  .add('default', () => (
    <Button label="Button" onClick={action('clicked')} />
  ))
  .add('disabled', () => (
    <Button label="Button" onClick={action('clicked')} isDisabled={true} />
  ));
