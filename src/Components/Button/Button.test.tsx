import React from 'react';
import { shallow, configure } from 'enzyme';
import toJson from 'enzyme-to-json';

import Button from './Button';
import Adapter from 'enzyme-adapter-react-16';

configure({
  adapter: new Adapter(),
});

describe('<Button />', () => {
  it('should match snapshot', () => {
    const rendered = shallow(<Button label="testButton" />);
    expect(toJson(rendered)).toMatchSnapshot();
  });

  it('shoud handle click callback when clicked', () => {
    const onClick = jest.fn();
    const wrapper = shallow(<Button label="testButton" onClick={onClick} />);
    const btn = wrapper.find('button').at(0);

    btn.simulate('click');
    btn.simulate('click');

    expect(onClick.mock.calls.length).toBe(2);
  });
});
