import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import TodoListItem, {TodoListItemProps} from './TodoListItem';

const defaultProps: TodoListItemProps = {
  isDone: false,
  label: "Storybook testing for 'TodoListItem' component",
  onRemove: action('onRemove'),
  onToggleIsDone: action('onToggleIsDone'),
};

storiesOf('TodoListItem', module)
  .add('default', () => (
    <React.Fragment>
      <TodoListItem {...defaultProps} />
      <TodoListItem {...defaultProps} />
      <TodoListItem {...defaultProps} />
    </React.Fragment>
  ))
  .add('done', () => (
    <React.Fragment>
      <TodoListItem {...defaultProps} isDone={true} />
      <TodoListItem {...defaultProps} isDone={true} />
      <TodoListItem {...defaultProps} isDone={true} />
    </React.Fragment>
  ));
