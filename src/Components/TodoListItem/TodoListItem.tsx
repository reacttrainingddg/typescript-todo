import React from 'react';
import composeClassNames from 'classnames';

import Button from '../Button/Button';

import style from './TodoListItem.module.scss';

export interface TodoListItemProps {
  isDone: boolean;
  label: string;
  onRemove: () => void;
  onToggleIsDone: () => void;
}

class TodoListItem extends React.PureComponent<TodoListItemProps> {
  public render(): JSX.Element {
    const { isDone, label, onRemove} = this.props;
    const finalClassName = composeClassNames(style['todo-list-item'], {
      [style['todo-list-item--done']]: isDone,
    });

    return (
      <li className={finalClassName}>
        <input type="checkbox" checked={isDone} onChange={this.handleToggleIsDone} />
        <p className={style['todo-list-item__description']}>{label}</p>
        <Button onClick={onRemove} label="x" />
      </li>
    );

  }

  private handleToggleIsDone = () => {
    this.props.onToggleIsDone();
  }
}

export default TodoListItem;
